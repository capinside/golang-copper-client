package opportunities

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	deleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "Delete opportunity",
		RunE:  deleteRunEFunc,
	}
)

func init() {
	deleteCmd.Flags().Int64VarP(&maximumCreatedDate, "maximum-created-date", "", -1, "The Unix timestamp of the latest date a resource was created.")
	deleteCmd.Flags().Int64VarP(&maximumInteractionDate, "maximum-interaction-date", "", -1, "The Unix timestamp of the latest date of the last interaction.")
	deleteCmd.Flags().Int64VarP(&minimumCreatedDate, "minimum-created-date", "", -1, "The Unix timestamp of the earliest date the resource was created.")
	deleteCmd.Flags().Int64VarP(&minimumInteractionDate, "minimum-interaction-date", "", -1, "The Unix timestamp of the earliest date of the last interaction.")
	deleteCmd.Flags().Int64VarP(&maximumInteractionCount, "maximum-interaction-count", "", -1, "The maximum number of interactions the resource must have had.")
}

func deleteRunEFunc(cmd *cobra.Command, args []string) error {
	opportunityIDs, err := opportunityIDs(args)
	if err != nil {
		return err
	}

	resp, err := delete(opportunityIDs)
	if err != nil {
		return err
	}

	return outputDeleteResponse(resp)
}

func opportunityIDs(args []string) ([]int64, error) {
	if len(args) > 0 {
		return utils.StringSliceToInt64Slice(args)
	}

	opportunities, err := api.SearchAllOpportunities(searchParams)
	if err != nil {
		return nil, err
	}

	result := make([]int64, len(opportunities))
	for i, op := range opportunities {
		result[i] = *op.Id
	}

	return result, nil
}

func delete(IDs []int64) ([]*copperv1.DeleteResponse, error) {
	result := make([]*copperv1.DeleteResponse, len(IDs))

	for i, id := range IDs {
		resp, err := api.DeleteOpportunity(id)
		if err != nil {
			return nil, err
		}
		result[i] = resp
	}

	return result, nil
}
