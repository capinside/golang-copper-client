package opportunities

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	opportunityCmd = &cobra.Command{
		Use:   "opportunities",
		Short: "Opportunity commands",
	}
)

func init() {
	opportunityCmd.AddCommand(
		deleteCmd,
		searchCmd,
	)
}

func Command() *cobra.Command {
	return opportunityCmd
}

func addSearchParams(cmd *cobra.Command) {
	cmd.Flags().Int64VarP(&maximumCreatedDate, "maximum-created-date", "", -1, "The Unix timestamp of the latest date a resource was created.")
	cmd.Flags().Int64VarP(&maximumInteractionDate, "maximum-interaction-date", "", -1, "The Unix timestamp of the latest date of the last interaction.")
	cmd.Flags().Int64VarP(&minimumCreatedDate, "minimum-created-date", "", -1, "The Unix timestamp of the earliest date the resource was created.")
	cmd.Flags().Int64VarP(&minimumInteractionDate, "minimum-interaction-date", "", -1, "The Unix timestamp of the earliest date of the last interaction.")
	cmd.Flags().Int64VarP(&maximumInteractionCount, "maximum-interaction-count", "", -1, "The maximum number of interactions the resource must have had.")
	cmd.Flags().Int64VarP(&minimumInteractionCount, "minimum-interaction-count", "", -1, "The minimum number of interactions the resource must have had.")
	cmd.PersistentFlags().StringVarP(&searchParams.Name, "name", "", "", "Full name of the Company to search for.")
	cmd.PersistentFlags().Int64SliceVarP(&searchParams.PipelineIds, "pipeline-ids", "", nil, "An array of pipeline IDs.")
}

func output(opportunities []*copperv1.Opportunity) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(opportunities)
	default:
		return outputJSON(opportunities)
	}
}

func outputWide(opportunities []*copperv1.Opportunity) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\t%s\n", "ID", "Name", "Status")

	for _, opportunity := range opportunities {
		fmt.Fprintf(w, "%d\t%s\t%s\n", opportunity.Id, opportunity.Name, opportunity.Status)
	}

	return nil
}

func outputJSON(opportunities []*copperv1.Opportunity) error {
	return json.NewEncoder(os.Stdout).Encode(opportunities)
}

func outputDeleteResponse(delResponses []*copperv1.DeleteResponse) error {
	switch viper.GetString("output") {
	case "wide":
		return outputDeleteResponseWide(delResponses)
	default:
		return outputDeleteResponseJSON(delResponses)
	}
}

func outputDeleteResponseJSON(delResponses []*copperv1.DeleteResponse) error {
	return json.NewEncoder(os.Stdout).Encode(delResponses)
}

func outputDeleteResponseWide(delResponses []*copperv1.DeleteResponse) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\n", "ID", "Deleted")

	for _, delResponse := range delResponses {
		fmt.Fprintf(w, "%d\t%v\n", delResponse.Id, delResponse.IsDeleted)
	}

	return nil
}
