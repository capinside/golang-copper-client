package customfields

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
)

var (
	listCmd = &cobra.Command{
		Use:   "list",
		Short: "List custom field defintions",
		RunE:  listRunEFunc,
	}
)

func listRunEFunc(cmd *cobra.Command, args []string) error {
	result, err := api.GetCustomFieldDefinitions()
	if err != nil {
		return err
	}

	return output(result)
}
