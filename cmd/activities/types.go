package activities

import (
	"encoding/json"
	"fmt"
	"io"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	typesCmd = &cobra.Command{
		Use:   "types",
		Short: "Show activity types",
		RunE:  typesRunEFunc,
	}
)

func typesRunEFunc(cmd *cobra.Command, args []string) error {
	types, err := api.ActivitiesTypes()
	if err != nil {
		return err
	}
	return outputTypes(*types, outputWriter)
}

func outputTypes(types copperv1.ActivityTypes, w io.Writer) error {
	switch viper.GetString("output") {
	case "wide":
		return outputTypesWide(types, w)
	default:
		return outputTypesJSON(types, w)
	}
}

func outputTypesJSON(types copperv1.ActivityTypes, w io.Writer) error {
	return json.NewEncoder(w).Encode(types)
}

func outputTypesWide(types copperv1.ActivityTypes, w io.Writer) error {
	tw := tabwriter.NewWriter(w, 10, 8, 0, '\t', 0)
	defer tw.Flush()

	fmt.Fprintf(tw, "%s\t%s\t%s\n", "ID", "Category", "Name")

	for _, t := range types.System {
		fmt.Fprintf(tw, "%d\t%s\t%s\n", t.Id, t.Category, t.Name)
	}

	for _, t := range types.User {
		fmt.Fprintf(tw, "%d\t%s\t%s\n", t.Id, t.Category, t.Name)
	}

	return nil
}
