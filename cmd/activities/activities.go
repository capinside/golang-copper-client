package activities

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	outputWriter io.Writer = os.Stdout

	activitiesCmd = &cobra.Command{
		Use:   "activities",
		Short: "Activity commands",
	}
)

func init() {
	activitiesCmd.AddCommand(
		fetchByIDCmd,
		searchCmd,
		typesCmd,
	)
}

// Command returns a pointer to the Cobra Command
func Command() *cobra.Command {
	return activitiesCmd
}

func output(activities []*copperv1.Activity, w io.Writer) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(activities, w)
	default:
		return outputJSON(activities, w)
	}
}

func outputWide(activities []*copperv1.Activity, w io.Writer) error {
	return fmt.Errorf("not yet implemented")
}

func outputJSON(activities []*copperv1.Activity, w io.Writer) error {
	return json.NewEncoder(w).Encode(activities)
}
