package activities

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	maximumActivityDate string
	minimumActivityDate string
	parentID            int64
	parentType          string
	timeFormat          string = time.RFC822
	typeCategory        string
	typeID              int64

	searchParams = copperv1.ActivitySearchParams{
		Parent: &copperv1.ActivityParent{},
	}
	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search for activities",
		RunE:  searchRunEFunc,
	}
)

func init() {
	searchCmd.Flags().StringVar(&searchParams.Parent.Type, "parent-type", "", "can be 'lead', 'person', 'company', 'opportunity', 'project', 'task'")
	searchCmd.Flags().StringVar(&typeCategory, "type-category", "", fmt.Sprintf("Activity type to filter. Must be one of: %s | %s", api.ActivityTypeCategorySystem, api.ActivityTypeCategoryUser))
	searchCmd.Flags().Int64Var(&searchParams.Parent.Id, "parent-id", 0, "ID of parent")
	searchCmd.Flags().Int64Var(&typeID, "type-id", 0, "ID of activity type to filter")
	searchCmd.Flags().StringVar(&maximumActivityDate, "maximum-activities-date", "", "The Unix timestamp of the latest activity date")
	searchCmd.Flags().StringVar(&minimumActivityDate, "minimum-activities-date", "", "The Unix timestamp of the earliest activity date")
	searchCmd.Flags().StringVar(&timeFormat, "time-format", timeFormat, "Layout to parse time flags")
}

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	if maximumActivityDate != "" {
		maximumActivityDateTime, err := time.Parse(timeFormat, maximumActivityDate)
		if err != nil {
			return err
		}
		t := maximumActivityDateTime.Unix()
		searchParams.MaximumActivityDate = &t
	}

	if minimumActivityDate != "" {
		minimumActivityDateTime, err := time.Parse(timeFormat, minimumActivityDate)
		if err != nil {
			return err
		}
		t := minimumActivityDateTime.Unix()
		searchParams.MinimumActivityDate = &t
	}

	if typeCategory != "" && typeID != 0 {
		searchParams.ActivityTypes = []*copperv1.ActivityType{
			{
				Category: typeCategory,
				Id:       typeID,
			},
		}
	}

	result, err := api.SearchAllActivities(searchParams)
	if err != nil {
		return err
	}

	return output(result, outputWriter)
}
