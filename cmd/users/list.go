package users

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search users",
		RunE:  searchRunEFunc,
	}
)

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	users, err := api.SearchAllUsers(copperv1.UserSearchParams{})
	if err != nil {
		return err
	}

	return output(users)
}
