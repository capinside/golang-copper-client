package leads

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"text/tabwriter"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	activitiesCmd = &cobra.Command{
		Use:   "activities",
		Short: "Lead activities",
	}

	activitiesListCmd = &cobra.Command{
		Use:   "list",
		Short: "List lead activities",
		RunE:  listRunEFunc,
	}
)

func init() {
	activitiesCmd.AddCommand(activitiesListCmd)
}

func listRunEFunc(cmd *cobra.Command, args []string) error {
	id, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	activities, err := api.LeadActivities(int64(id), copperv1.ActivitySearchParams{})
	if err != nil {
		return err
	}

	return outputActivities(activities)
}

func outputActivities(activities []*copperv1.Activity) error {
	switch viper.GetString("output") {
	case "wide":
		return outputActivitiesWide(activities)
	default:
		return outputActivitiesJSON(activities)
	}
}

func outputActivitiesJSON(activities []*copperv1.Activity) error {
	return json.NewEncoder(os.Stdout).Encode(activities)
}

func outputActivitiesWide(activities []*copperv1.Activity) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\n", "Activity ID", "Activity date")
	for _, activity := range activities {
		fmt.Fprintf(w, "%d\t%s\n", activity.Id, time.Unix(*activity.ActivityDate, 0))
	}

	return nil
}
