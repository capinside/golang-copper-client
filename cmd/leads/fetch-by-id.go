package leads

import (
	"fmt"

	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByID = &cobra.Command{
		Use:   "fetch-by-id",
		Short: "Fetch lead by ID",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return fmt.Errorf("argument required")
			}
			return nil
		},
		RunE: fetchByIDRunEFunc,
	}
)

func fetchByIDRunEFunc(cmd *cobra.Command, args []string) error {
	ids, err := utils.StringSliceToInt64Slice(args)
	if err != nil {
		return err
	}

	result := make([]*copperv1.Lead, len(ids))
	for i, id := range ids {
		lead, err := api.FetchLeadByID(id)
		if err != nil {
			return err
		}

		result[i] = lead
	}

	return output(result)
}
