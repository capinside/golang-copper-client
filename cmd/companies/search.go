package companies

import (
	"fmt"

	"github.com/spf13/cobra"

	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	maximumCreatedDate      int64
	maximumInteractionCount int64
	maximumInteractionDate  int64
	minimumCreatedDate      int64
	minimumInteractionCount int64
	minimumInteractionDate  int64

	searchParams = copperv1.CompanySearchParams{}

	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search companies",
		RunE:  searchRunEFunc,
	}
)

func init() {
	addSearchParams(searchCmd)
}

func addSearchParams(cmd *cobra.Command) {
	cmd.Flags().Int64VarP(&maximumCreatedDate, "maximum-created-date", "", -1, "The Unix timestamp of the latest date a resource was created.")
	cmd.Flags().Int64VarP(&maximumInteractionDate, "maximum-interaction-date", "", -1, "The Unix timestamp of the latest date of the last interaction.")
	cmd.Flags().Int64VarP(&minimumCreatedDate, "minimum-created-date", "", -1, "The Unix timestamp of the earliest date the resource was created.")
	cmd.Flags().Int64VarP(&minimumInteractionDate, "minimum-interaction-date", "", -1, "The Unix timestamp of the earliest date of the last interaction.")
	cmd.Flags().Int64VarP(&maximumInteractionCount, "maximum-interaction-count", "", -1, "The maximum number of interactions the resource must have had.")
	cmd.Flags().Int64VarP(&minimumInteractionCount, "minimum-interaction-count", "", -1, "The minimum number of interactions the resource must have had.")
	cmd.Flags().StringVarP(&searchParams.City, "city", "", "", "The city in which Company must be located.")
	cmd.Flags().StringVarP(&searchParams.EmailDomain, "email-domain", "", "", "Email domain of the Company to search for.")
	cmd.Flags().StringVarP(&searchParams.Name, "name", "", "", "Full name of the Company to search for.")
}

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	companies, err := api.SearchAllCompanies(searchParams)
	if err != nil {
		return fmt.Errorf("search all companies: %v", err)
	}

	return output(companies)
}
