package companies

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	deleteUnused bool

	deleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "Delete company",
		RunE:  deleteRunEFunc,
	}
)

func init() {
	addSearchParams(deleteCmd)

	deleteCmd.Flags().BoolVarP(&deleteUnused, "unused", "", false, "Delete companies without interactions and leads")
}

func deleteRunEFunc(cmd *cobra.Command, args []string) error {
	var companyIDs []int64
	var err error

	if deleteUnused {
		companies, err := unusedCompanies()
		if err != nil {
			return err
		}
		companyIDs = make([]int64, len(companies))
		for i, company := range companies {
			companyIDs[i] = *company.Id
		}
	} else {
		companyIDs, err = utils.StringSliceToInt64Slice(args)
		if err != nil {
			return err
		}
	}

	if viper.GetBool("dry-run") {
		return nil
	}

	resp, err := deleteCompaniesByID(companyIDs)
	if err != nil {
		return err
	}

	return outputDeleteResponse(resp)
}

func deleteCompaniesByID(IDs []int64) ([]*copperv1.DeleteResponse, error) {
	result := make([]*copperv1.DeleteResponse, len(IDs))

	for i, id := range IDs {
		resp, err := api.DeleteCompany(id)
		if err != nil {
			return nil, err
		}
		result[i] = resp
	}

	return result, nil
}

func unusedCompanies() ([]*copperv1.Company, error) {
	companies, err := api.SearchAllCompanies(searchParams)
	if err != nil {
		return nil, fmt.Errorf("search all companies")
	}

	var result []*copperv1.Company
	for _, company := range companies {
		personSearchParams := copperv1.PersonSearchParams{
			CompanyIds: []int64{*company.Id},
		}
		persons, err := api.SearchAllPersons(personSearchParams)
		if err != nil {
			return nil, fmt.Errorf("search all persons: %v", err)
		}

		if len(persons) == 0 && company.InteractionCount != nil && *company.InteractionCount == 0 {
			result = append(result, company)
		}
	}

	return result, nil
}
