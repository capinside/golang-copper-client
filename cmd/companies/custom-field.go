package companies

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/capinside/copper-cli/pkg/copper"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	customField                            = copperv1.CustomField{}
	customFieldCompanySearchAssignee int64 = -2
	customFieldValue                 string

	customFieldCmd = &cobra.Command{
		Use:   "custom-field",
		Short: "Set custom field value",
		RunE:  customFieldRunEFunc,
	}
)

func init() {
	customFieldCmd.Flags().Int64VarP(&customFieldCompanySearchAssignee, "assignee", "", -2, "ID of Copper user assigned as the owner of companies to search for")
	customFieldCmd.Flags().Int64VarP(&customField.CustomFieldDefinitionId, "custom-field-definition-id", "", 0, "ID of Copper custom field definition to set")
	customFieldCmd.Flags().StringVarP(&customFieldValue, "value", "", "", "Value to set")
}

func customFieldRunEFunc(cmd *cobra.Command, args []string) error {
	customFieldDefinition, err := api.FetchCustomFieldDefinitionByID(customField.CustomFieldDefinitionId)
	if err != nil {
		return fmt.Errorf("fetch custom field definition by ID: %v", err)
	}

	if !copper.IsCustomFieldDefinitionAvailableOn(*customFieldDefinition, "company") {
		return fmt.Errorf("custom field definition '%d' not available for data type company", customFieldDefinition.Id)
	}

	// var value copperv1.Any
	// switch customFieldDefinition.DataType {
	// case "Checkbox", "Dropdown", "Float":
	// 	// value, parseErr := strconv.ParseFloat(customFieldValue, 64)
	// 	// if parseErr == nil {
	// 	// 	err = anycopperv1.UnmarshalTo(customFieldValue, &value, proto.UnmarshalOptions{})
	// 	// }
	// case "String", "Text", "URL":
	// 	// value = string(customFieldValue)
	// 	// err = nil
	// default:
	// 	err = fmt.Errorf("data type '%s' not yet supported", customFieldDefinition.DataType)
	// }
	// if err != nil {
	// 	return fmt.Errorf("convert format value '%v' into Copper data type '%s': %v", value, customFieldDefinition.DataType, err)
	// }
	// customField.Value = &value

	searchParams.AssigneeIds = []int64{customFieldCompanySearchAssignee}
	companies, err := api.SearchAllCompanies(searchParams)
	if err != nil {
		return fmt.Errorf("search all companies: %v", err)
	}
	for _, company := range companies {
		_, err := api.UpdateCompany(*company.Id, copperv1.Company{
			CustomFields: []*copperv1.CustomField{&customField},
		})
		if err != nil {
			return fmt.Errorf("update company '%s': %v", company.Name, err)
		}
	}

	return nil
}
