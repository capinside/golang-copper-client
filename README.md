# Copper Golang client

Golang API client to work with [Copper API](https://developer.copper.com/index.html).

For each entity a service can be created to interact with the entity (see example below).

## Installation

```shell
$ go install gitlab.com/capinside/copper-cli
```

## Usage

Following environment variables are used by the client:

| Name               | Description               | Default                                  | Required |
| :----------------- | :------------------------ | :--------------------------------------- | :------: |
| COPPER_API_URL     | URL to reach Copper API   | `https://api.copper.com/developer_api/v` | `false`  |
| COPPER_API_ACCOUNT | Account used for requests | `""`                                     |  `true`  |
| COPPER_API_TOKEN   | Token used for requests   | `""`                                     |  `true`  |


### Command line utility

```shell
$ copper-cli -h

Copper command line utitliy

Usage:
  copper-cli [command]

Available Commands:
  activities       Activity commands
  companies        Company commands
  completion       Generate the autocompletion script for the specified shell
  custom-fields    Custom field commands
  customer-sources Customer sources commands
  help             Help about any command
  leads            Lead commands
  opportunities    Opportunity commands
  persons          Person commands
  pipelines        Pipeline commands
  users            User commands

Flags:
      --dry-run         Run in dry-run mode
  -h, --help            help for copper-cli
  -o, --output string   Output format. Must be one of: json | wide (default "json")
```

### Use the default initialized Client

```go
package main

import (
	"fmt"

	"gitlab.com/capinside/copper-cli/pkg/copper"
)

func main() {
	leads,err := copper.Leads.SearchAll(copper.LeadSearchParams{})
	if err != nil {
		panic(err)
	}

	fmt.Println(leads)
}
```

### Initialize a Client 

```go

package main

import (
	"fmt"
    "os"

	"gitlab.com/capinside/copper-cli/pkg/copper"
)

func main() {
	// init config
	config, err := copper.NewConfig(
		os.Getenv("COPPER_API_URL"),
		os.Getenv("COPPER_API_ACCOUNT"),
		os.Getenv("COPPER_API_TOKEN"),
	)
	if err != nil {
		panic(err)
	}

	// init client
	client := copper.NewClient(*config)

	// create leads service
	leadsService := copper.NewLeadsService(client)

	// search leads
	leads, err := copper.Leads.Search(copper.LeadSearchParams{})
	if err != nil {
		panic(err)
	}

	fmt.Println(leads)
}
```

## Note 

Only a limited set of all API functions are currently implemented, however the existing framework makes implementing further functions extremely lightweight and easy.

## Contributing

Bug reports and pull requests are welcome. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](./CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).


## Author

[CAPinside GmbH](https://capinside.com)