package copper

import (
	"strconv"
	"strings"
)

// StringSliceToIntSlice converts s into a slice of int and returns the slice of int's and nil or nil and an error
func StringSliceToIntSlice(s []string) ([]int, error) {
	result := make([]int, len(s))
	for i, v := range s {
		id, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}

		result[i] = id
	}

	return result, nil
}

// StringSliceToInt64Slice converts s into a slice of int and returns the slice of int's and nil or nil and an error
func StringSliceToInt64Slice(s []string) ([]int64, error) {
	result := make([]int64, len(s))
	for i, v := range s {
		id, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}

		result[i] = int64(id)
	}

	return result, nil
}

// StringInStringSlice returns true if 'name' can be found in 's' else false
func StringInStringSlice(name string, s []string) bool {
	for _, item := range s {
		if strings.EqualFold(item, name) {
			return true
		}
	}
	return false
}

// IntAddr returns a pointer of i
func IntAddr(i int) *int {
	return &i
}

// Int64Addr returns a pointer of i
func Int64Addr(i int64) *int64 {
	return &i
}

// Float64Addr returns a pointer of f
func Float64Addr(f float64) *float64 {
	return &f
}

// StringAddr returns a pointer of s
func StringAddr(s string) *string {
	return &s
}
