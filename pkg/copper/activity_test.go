package copper

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestActivityUnmarshalJSON(t *testing.T) {
	tt := []struct {
		Data   []byte
		Result *copperv1.Activity
		Error  error
	}{
		{
			Data:   []byte{},
			Result: &copperv1.Activity{},
			Error:  fmt.Errorf("unexpected end of JSON input"),
		},
		{
			Data: []byte(`{"details":"test detail"}`),
			Result: &copperv1.Activity{
				Details: "test detail",
			},
			Error: nil,
		},
		{
			// Data: []byte(`{"details":{"recipients":[{"email":"someone@something.net"},{"email":"someone-else@something.net"}],"sender":{"email":"johndoe@example.net","name":"John Doe"},"subject":"test subject"}}`),
			Data: []byte(`{"details":"test details","recipients":[{"email":"someone@something.net"},{"email":"someone-else@something.net"}],"sender":{"email":"johndoe@example.net","name":"John Doe"},"subject":"test subject"}`),
			Result: &copperv1.Activity{
				Details: "test details",
				Recipients: []*copperv1.ActivityDetailsRecipient{
					{
						Email: "someone@something.net",
					},
					{
						Email: "someone-else@something.net",
					},
				},
				Sender: &copperv1.ActivityDetailsSender{
					Email: "johndoe@example.net",
					Name:  "John Doe",
				},
				Subject: "test subject",
			},
			Error: nil,
		},
		{
			Data: []byte(`{"activity_date":0,"date_created":12345,"date_modified":23456,"id":1,"details":"","parent":{"id":1,"type":"test"},"type":{"category":"test category","count_as_interaction":true,"id":1,"is_disabled":true,"name":"test name"}}`),
			Result: &copperv1.Activity{
				ActivityDate: utils.Int64Addr(0),
				Id:           utils.Int64Addr(1),
				DateCreated:  utils.Int64Addr(12345),
				DateModified: utils.Int64Addr(23456),
				// NewValue:     proto.String("foo"),
				// OldValue:     proto.String("bar"),
				Parent: &copperv1.ActivityParent{
					Id:   1,
					Type: "test",
				},
				Type: &copperv1.ActivityType{
					Category:           "test category",
					CountAsInteraction: true,
					Id:                 1,
					IsDisabled:         true,
					Name:               "test name",
				},
			},
		},
		{
			Data: []byte(`{"details":null}`),
			Result: &copperv1.Activity{
				Details: "",
			},
		},
	}

	for i, test := range tt {
		result := &copperv1.Activity{}
		err := json.Unmarshal(test.Data, result)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}
	}
}

func TestNewActivitiesService(t *testing.T) {
	as := NewActivitiesService(nil)
	assert.NotNil(t, as)
}

func TestActivitiesTypes(t *testing.T) {
	tt := []struct {
		Body       []byte
		Result     *copperv1.ActivityTypes
		StatusCode int
		Error      error
	}{
		{
			Body:       []byte(`{}`),
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Body: []byte(`{"user":[{"id":0,"category":"user","name":"Note","is_disabled":false,"count_as_interaction":false},{"id":190711,"category":"user","name":"Phone Call","is_disabled":false,"count_as_interaction":true},{"id":190712,"category":"user","name":"Meeting","is_disabled":false,"count_as_interaction":true},{"id":191400,"category":"user","name":"Demo call","is_disabled":false,"count_as_interaction":true},{"id":194674,"category":"user","name":"Call - no connect","is_disabled":false,"count_as_interaction":true}],"system":[{"id":1,"category":"system","name":"Property Changed","is_disabled":false,"count_as_interaction":false},{"id":3,"category":"system","name":"Pipeline Stage Changed","is_disabled":false,"count_as_interaction":false}]}`),
			Result: &copperv1.ActivityTypes{
				User: []*copperv1.ActivityType{
					{
						Id:                 0,
						Category:           "user",
						Name:               "Note",
						IsDisabled:         false,
						CountAsInteraction: false,
					},
					{
						Id:                 190711,
						Category:           "user",
						Name:               "Phone Call",
						IsDisabled:         false,
						CountAsInteraction: true,
					},
					{
						Id:                 190712,
						Category:           "user",
						Name:               "Meeting",
						IsDisabled:         false,
						CountAsInteraction: true,
					},
					{
						Id:                 191400,
						Category:           "user",
						Name:               "Demo call",
						IsDisabled:         false,
						CountAsInteraction: true,
					},
					{
						Id:                 194674,
						Category:           "user",
						Name:               "Call - no connect",
						IsDisabled:         false,
						CountAsInteraction: true,
					},
				},
				System: []*copperv1.ActivityType{
					{
						Id:                 1,
						Category:           "system",
						Name:               "Property Changed",
						IsDisabled:         false,
						CountAsInteraction: false,
					},
					{
						Id:                 3,
						Category:           "system",
						Name:               "Pipeline Stage Changed",
						IsDisabled:         false,
						CountAsInteraction: false,
					},
				},
			},
			StatusCode: http.StatusOK,
		},
	}

	for _, test := range tt {
		server := setupTest(ActivityTypesPath, test.StatusCode, test.Body)

		result, err := ActivitiesTypes()
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestSearchAllActivities(t *testing.T) {
	tt := []struct {
		Params     copperv1.ActivitySearchParams
		Body       []byte
		Result     []*copperv1.Activity
		StatusCode int
		Error      error
	}{
		{
			Body:       []byte("{}"),
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Params: copperv1.ActivitySearchParams{},
			// Body:   []byte(`[{"id":3064242278,"parent":{"id":27140359,"type":"person"},"type":{"id":0,"category":"user"},"user_id":137658,"details":"test detail","activity_date":1496772355,"old_value":"123","new_value":345,"date_created":1496772355,"date_modified":1496772355},{"id":3061844454,"parent":{"id":9607580,"type":"company"},"type":{"id":190711,"category":"user"},"user_id":137658,"details":{"sender":{"email":"johndoe@example.com","name":"John Doe"},"recipients":[{"email":"someone@example.net"}],"subject":"test subject","id":12345},"activity_date":1496710783,"old_value":null,"new_value":null,"date_created":1496710787,"date_modified":1496710783}]`),
			Body: []byte(`[
				{"id":3064242278,"parent":{"id":27140359,"type":"person"},"type":{"id":0,"category":"user"},"user_id":137658,"details":"test detail","activity_date":1496772355,"date_created":1496772355,"date_modified":1496772355},
				{"id":3061844454,"parent":{"id":9607580,"type":"company"},"type":{"id":190711,"category":"user"},"user_id":137658,"details":"","activity_date":1496710783,"date_created":1496710787,"date_modified":1496710783,"sender":{"email":"johndoe@example.com","name":"John Doe"},"recipients":[{"email":"someone@example.net"}],"subject":"test subject"}]`),
			Result: []*copperv1.Activity{
				{
					Id: utils.Int64Addr(3064242278),
					Parent: &copperv1.ActivityParent{
						Id:   27140359,
						Type: "person",
					},
					Type: &copperv1.ActivityType{
						Id:       0,
						Category: "user",
					},
					UserId:       utils.Int64Addr(137658),
					Details:      "test detail",
					ActivityDate: utils.Int64Addr(1496772355),
					DateCreated:  utils.Int64Addr(1496772355),
					DateModified: utils.Int64Addr(1496772355),
					// OldValue:     "123",
					// NewValue:     float64(345),
				},
				{
					Id: utils.Int64Addr(3061844454),
					Parent: &copperv1.ActivityParent{
						Id:   9607580,
						Type: "company",
					},
					Type: &copperv1.ActivityType{
						Id:       190711,
						Category: "user",
					},
					UserId:  utils.Int64Addr(137658),
					Details: "",
					Sender: &copperv1.ActivityDetailsSender{
						Email: "johndoe@example.com",
						Name:  "John Doe",
					},
					Recipients: []*copperv1.ActivityDetailsRecipient{
						{
							Email: "someone@example.net",
						},
					},
					Subject:      "test subject",
					ActivityDate: utils.Int64Addr(1496710783),
					DateCreated:  utils.Int64Addr(1496710787),
					DateModified: utils.Int64Addr(1496710783),
				},
			},
			StatusCode: http.StatusOK,
		},
	}
	for i, test := range tt {
		server := setupTest(ActivitiesSearchPath, test.StatusCode, test.Body)

		result, err := SearchAllActivities(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()
	}
}

func TestDeleteActivity(t *testing.T) {
	tt := []struct {
		ID         int64
		Result     *copperv1.DeleteResponse
		Body       []byte
		StatusCode int
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Body:       []byte("{}"),
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID:         1,
			StatusCode: http.StatusOK,
			Body:       []byte(`{"id":1,"is_deleted":true}`),
			Result: &copperv1.DeleteResponse{
				Id:        1,
				IsDeleted: true,
			},
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", ActivitiesPath, test.ID), test.StatusCode, test.Body)

		result, err := DeleteActivity(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestFetchActivityByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Result     *copperv1.Activity
		Body       []byte
		StatusCode int
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID:         1,
			StatusCode: http.StatusOK,
			Body:       []byte(`{"id":1}`),
			Result: &copperv1.Activity{
				Id: utils.Int64Addr(1),
			},
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", ActivitiesPath, test.ID), test.StatusCode, test.Body)

		result, err := FetchActivityByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestIsSystemActivity(t *testing.T) {
	tt := []struct {
		Activity copperv1.Activity
		Result   bool
	}{
		{
			Activity: copperv1.Activity{
				Type: &copperv1.ActivityType{
					Category: ActivityTypeCategorySystem,
				},
			},
			Result: true,
		},
		{
			Activity: copperv1.Activity{
				Type: &copperv1.ActivityType{
					Category: ActivityTypeCategoryUser,
				},
			},
			Result: false,
		},
		{
			Activity: copperv1.Activity{
				Type: &copperv1.ActivityType{
					Category: "foobar",
				},
			},
			Result: false,
		},
	}

	for _, test := range tt {
		assert.Equal(t, test.Result, IsSystemActivity(test.Activity))
	}
}
func TestIsUserActivity(t *testing.T) {
	tt := []struct {
		Activity copperv1.Activity
		Result   bool
	}{
		{
			Activity: copperv1.Activity{
				Type: &copperv1.ActivityType{
					Category: ActivityTypeCategorySystem,
				},
			},
			Result: false,
		},
		{
			Activity: copperv1.Activity{
				Type: &copperv1.ActivityType{
					Category: ActivityTypeCategoryUser,
				},
			},
			Result: true,
		},
		{
			Activity: copperv1.Activity{
				Type: &copperv1.ActivityType{
					Category: "foobar",
				},
			},
			Result: false,
		},
	}

	for _, test := range tt {
		assert.Equal(t, test.Result, IsUserActivity(test.Activity))
	}
}
