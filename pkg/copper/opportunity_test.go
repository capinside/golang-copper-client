package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewOpportunitiesService(t *testing.T) {
	os := NewOpportunitiesService(nil)
	assert.NotNil(t, os)
}

func TestCreateOpportunity(t *testing.T) {
	tt := []struct {
		Body       []byte
		Data       copperv1.Opportunity
		Result     *copperv1.Opportunity
		StatusCode int
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Body: []byte(`{"id":4956209,"name":"New Demo copperv1.Opportunity","assignee_id":null,"close_date":null,"company_id":13349319,"company_name":"Noemail","customer_source_id":331242,"details":null,"loss_reason_id":null,"pipeline_id":213214,"pipeline_stage_id":987790,"primary_contact_id":27140359,"priority":"None","status":"Open","tags":["one","two"],"interaction_count":0,"monetary_value":null,"win_probability":5,"date_last_contacted":null,"leads_converted_from":[],"date_created":1502158599,"date_modified":1502158599,"custom_fields":[{"custom_field_definition_id":126240,"value":null},{"custom_field_definition_id":103481,"value":null},{"custom_field_definition_id":100764,"value":null}]}`),
			Data: copperv1.Opportunity{},
			Result: &copperv1.Opportunity{
				CompanyId:        utils.Int64Addr(13349319),
				CompanyName:      "Noemail",
				CustomerSourceId: utils.Int64Addr(331242),
				CustomFields: []*copperv1.CustomField{
					{
						CustomFieldDefinitionId: 126240,
					},
					{
						CustomFieldDefinitionId: 103481,
					},
					{
						CustomFieldDefinitionId: 100764,
					},
				},
				DateCreated:      utils.Int64Addr(1502158599),
				DateModified:     utils.Int64Addr(1502158599),
				Id:               utils.Int64Addr(4956209),
				InteractionCount: utils.Int64Addr(0),
				Name:             "New Demo copperv1.Opportunity",
				PipelineId:       213214,
				PipelineStageId:  987790,
				PrimaryContactId: 27140359,
				Priority:         "None",
				Status:           "Open",
				Tags:             []string{"one", "two"},
				WinProbability:   5,
			},
			StatusCode: http.StatusOK,
			Error:      nil,
		},
	}

	for _, test := range tt {
		server := setupTest(OpportunitiesPath, test.StatusCode, test.Body)

		result, err := CreateOpportunity(test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestDeleteOpportunity(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		Result     *copperv1.DeleteResponse
		StatusCode int
		Error      error
	}{
		{
			ID:         1234567890,
			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			StatusCode: http.StatusNotFound,
			Error:      ErrResourceNotFound,
		},
		{
			ID:         71258463,
			Body:       []byte(`{"id":71258463,"is_deleted":true}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.DeleteResponse{
				Id:        71258463,
				IsDeleted: true,
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", OpportunitiesPath, test.ID), test.StatusCode, test.Body)

		result, err := DeleteOpportunity(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestUpdateOpportunity(t *testing.T) {
	tt := []struct {
		ID         int64
		Data       copperv1.Opportunity
		Body       []byte
		Result     *copperv1.Opportunity
		StatusCode int
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID: 1,
			Data: copperv1.Opportunity{
				Name:          "John Doe copperv1.Opportunity",
				MonetaryValue: utils.Float64Addr(1000000),
			},
			Body:       []byte(`{"id":1,"name":"John Doe copperv1.Opportunity","monetary_value":1000000}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Opportunity{
				Id:            utils.Int64Addr(1),
				Name:          "John Doe copperv1.Opportunity",
				MonetaryValue: utils.Float64Addr(1000000),
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", OpportunitiesPath, test.ID), test.StatusCode, test.Body)

		result, err := UpdateOpportunity(test.ID, test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestSearchOpportunities(t *testing.T) {
	tt := []struct {
		Params     copperv1.OpportunitySearchParams
		Body       []byte
		Result     []*copperv1.Opportunity
		StatusCode int
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Params:     copperv1.OpportunitySearchParams{},
			Body:       []byte(`[{"id":1,"name":"John Doe copperv1.Opportunity","monetary_value":1000000}]`),
			StatusCode: http.StatusOK,
			Result: []*copperv1.Opportunity{
				{
					Id:            utils.Int64Addr(1),
					Name:          "John Doe copperv1.Opportunity",
					MonetaryValue: utils.Float64Addr(1000000),
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(OpportunitiesSearchPath, test.StatusCode, test.Body)

		result, err := SearchOpportunities(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}
