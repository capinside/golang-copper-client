package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// CompaniesPath on API
	CompaniesPath = "/companies"
	// CompaniesSearchPath on API
	CompaniesSearchPath = "/companies/search"
)

var (
	// Companies is the global companies service
	Companies CompaniesService
)

// CompaniesService interface
type CompaniesService interface {
	// Delete company matching id and return a pointer of DeleteResponse and nil
	// or nil and an error
	Delete(int64) (*copperv1.DeleteResponse, error)
	// Fetch company matching id and return a pointer of Company and nil
	// or nil and an error
	FetchByID(int64) (*copperv1.Company, error)
	// Search companies matching params and return a slice of Company and nil
	// or nil and an error. Does not handle pagination.
	Search(copperv1.CompanySearchParams) ([]*copperv1.Company, error)
	// SearchAll companies matching params and returns a slice if company and nil
	// or nil and an error. Handles pagination.
	SearchAll(copperv1.CompanySearchParams) ([]*copperv1.Company, error)
	// Update company matching id with data and return a pointer of Company and nil
	// or nil and an error
	Update(int64, copperv1.Company) (*copperv1.Company, error)
}

// CompaniesDeleteFunc function header
type CompaniesDeleteFunc func(int64) (*copperv1.DeleteResponse, error)

// CompaniesFetchByIDFunc function header
type CompaniesFetchByIDFunc func(int64) (*copperv1.Company, error)

// CompaniesSearchFunc function header
type CompaniesSearchFunc func(copperv1.CompanySearchParams) ([]*copperv1.Company, error)

// CompaniesUpdateFunc function header
type CompaniesUpdateFunc func(int64, copperv1.Company) (*copperv1.Company, error)

// NewCompaniesService returns an instance of companiesService that
// implements the CompaniesService interface
func NewCompaniesService(client Client) CompaniesService {
	return NewCompaniesServiceWithFunc(
		companiesDeleteFunc(client),
		companiesFetchByIDFunc(client),
		companiesSearchFunc(client),
		companiesUpdateFunc(client),
	)
}

// NewCompaniesServiceWithFunc returns an instance of companiesService that
// implements the CompaniesService interface and custom functions set
func NewCompaniesServiceWithFunc(deleteFunc CompaniesDeleteFunc, fetchByIDFunc CompaniesFetchByIDFunc, searchFunc CompaniesSearchFunc, updateFunc CompaniesUpdateFunc) CompaniesService {
	return &companiesService{
		deleteFunc:    deleteFunc,
		fetchByIDFunc: fetchByIDFunc,
		searchFunc:    searchFunc,
		updateFunc:    updateFunc,
	}
}

type companiesService struct {
	deleteFunc    CompaniesDeleteFunc
	fetchByIDFunc CompaniesFetchByIDFunc
	searchFunc    CompaniesSearchFunc
	updateFunc    CompaniesUpdateFunc
}

func companiesSearchFunc(client Client) CompaniesSearchFunc {
	return func(params copperv1.CompanySearchParams) ([]*copperv1.Company, error) {
		data, err := client.Post(CompaniesSearchPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Company{}
		return result, json.Unmarshal(data, &result)
	}
}

// Search returns a slice of Company matching params and nil or nil and an error
func (cs *companiesService) Search(params copperv1.CompanySearchParams) ([]*copperv1.Company, error) {
	return cs.searchFunc(params)
}

func (cs *companiesService) SearchAll(params copperv1.CompanySearchParams) ([]*copperv1.Company, error) {
	result := []*copperv1.Company{}

	params.PageNumber = 1
	params.PageSize = MaxRequestPageSize

	done := false
	for !done {

		companies, err := cs.Search(params)
		if err != nil {
			return nil, err
		}

		result = append(result, companies...)

		done = int64(len(companies)) < params.PageSize
		params.PageNumber++
	}

	return result, nil
}

func companiesFetchByIDFunc(client Client) CompaniesFetchByIDFunc {
	return func(id int64) (*copperv1.Company, error) {
		path := fmt.Sprintf("%s/%d", CompaniesPath, id)

		resp, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Company{}
		return result, json.Unmarshal(resp, &result)
	}
}

// FetchByID returns the company matching id and nil or nil and an error
func (cs *companiesService) FetchByID(id int64) (*copperv1.Company, error) {
	return cs.fetchByIDFunc(id)
}

func companiesUpdateFunc(client Client) CompaniesUpdateFunc {
	return func(id int64, data copperv1.Company) (*copperv1.Company, error) {
		url := fmt.Sprintf("%s/%d", CompaniesPath, id)

		resp, err := client.Put(url, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Company{}
		return result, json.Unmarshal(resp, &result)
	}
}

// Update company matching id with data and return a pointer of Company and nil or nil and an error
func (cs *companiesService) Update(id int64, data copperv1.Company) (*copperv1.Company, error) {
	return cs.updateFunc(id, data)
}

func companiesDeleteFunc(client Client) CompaniesDeleteFunc {
	return func(id int64) (*copperv1.DeleteResponse, error) {
		return deleteResourceByIDFunc(client)(CompaniesPath, id)
	}
}

// Delete company matching id and return a pointer of DeleteResponse and nil or nil and an error
func (cs *companiesService) Delete(id int64) (*copperv1.DeleteResponse, error) {
	return cs.deleteFunc(id)
}

// DeleteCompany deletes the company matching id and returns a pointer of DeleteResponse and nil
// or nil and an error.
func DeleteCompany(id int64) (*copperv1.DeleteResponse, error) {
	return Companies.Delete(id)
}

// FetchCompanyByID returns a  pointer of Company matching id and nil
// or nil and an error
func FetchCompanyByID(id int64) (*copperv1.Company, error) {
	return Companies.FetchByID(id)
}

// SearchCompanies returns a slice of Company matching params and nil
// or nil and an error. Does not handle pagination.
func SearchCompanies(params copperv1.CompanySearchParams) ([]*copperv1.Company, error) {
	return Companies.Search(params)
}

// SearchAllCompanies returns a slice of Company matching params and nil
// or nil and an error. Handles pagination.
func SearchAllCompanies(params copperv1.CompanySearchParams) ([]*copperv1.Company, error) {
	return Companies.SearchAll(params)
}

// UpdateCompany updates the Company matching id with data and returns a pointer of Company and nil
// or nil and an error
func UpdateCompany(id int64, data copperv1.Company) (*copperv1.Company, error) {
	return Companies.Update(id, data)
}
