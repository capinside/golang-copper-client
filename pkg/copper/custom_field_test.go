package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewCustomFieldDefinitionsService(t *testing.T) {
	cfds := NewCustomFieldDefinitionsService(nil)
	assert.NotNil(t, cfds)
}

func TestGetCustomFieldDefinitions(t *testing.T) {
	tt := []struct {
		Body       string
		StatusCode int
		Result     []*copperv1.CustomFieldDefinition
		Error      error
	}{
		{
			Body:       `[{"id":100764,"name":"A Text Field","data_type":"String","available_on":["company","opportunity","lead","person"]},{"id":103481,"name":"A Text Area Field","data_type":"Text","available_on":["lead","company","opportunity","person"]},{"id":126240,"name":"Color Option","data_type":"Dropdown","available_on":["opportunity","project"],"options":[{"id":167776,"name":"Yellow","rank":4},{"id":167775,"name":"Orange","rank":3},{"id":167774,"name":"Blue","rank":2},{"id":167773,"name":"Green","rank":1},{"id":167772,"name":"Red","rank":0}]}]`,
			StatusCode: http.StatusOK,
			Result: []*copperv1.CustomFieldDefinition{
				{
					Id:          utils.Int64Addr(100764),
					Name:        "A Text Field",
					DataType:    "String",
					AvailableOn: []string{"company", "opportunity", "lead", "person"},
				},
				{
					Id:          utils.Int64Addr(103481),
					Name:        "A Text Area Field",
					DataType:    "Text",
					AvailableOn: []string{"lead", "company", "opportunity", "person"},
				},
				{
					Id:          utils.Int64Addr(126240),
					Name:        "Color Option",
					DataType:    "Dropdown",
					AvailableOn: []string{"opportunity", "project"},
					Options: []*copperv1.CustomFieldDefinitionOption{
						{
							Id:   utils.Int64Addr(167776),
							Name: "Yellow",
							Rank: 4,
						},
						{
							Id:   utils.Int64Addr(167775),
							Name: "Orange",
							Rank: 3,
						},
						{
							Id:   utils.Int64Addr(167774),
							Name: "Blue",
							Rank: 2,
						},
						{
							Id:   utils.Int64Addr(167773),
							Name: "Green",
							Rank: 1,
						},
						{
							Id:   utils.Int64Addr(167772),
							Name: "Red",
							Rank: 0,
						},
					},
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(CustomFieldDefinitionsPath, test.StatusCode, []byte(test.Body))

		result, err := GetCustomFieldDefinitions()
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestFetchCustomFieldDefinitionByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       string
		StatusCode int
		Result     *copperv1.CustomFieldDefinition
		Error      error
	}{
		{
			ID:         100764,
			Body:       `{"id":100764,"name":"A Text Field","data_type":"String","available_on":["company","opportunity","lead","person"]}`,
			StatusCode: http.StatusOK,
			Result: &copperv1.CustomFieldDefinition{
				Id:          utils.Int64Addr(100764),
				Name:        "A Text Field",
				DataType:    "String",
				AvailableOn: []string{"company", "opportunity", "lead", "person"},
			},
		},
		{
			ID:         126240,
			Body:       `{"id":126240,"name":"Color Option","data_type":"Dropdown","available_on":["opportunity","project"],"options":[{"id":167776,"name":"Yellow","rank":4},{"id":167775,"name":"Orange","rank":3},{"id":167774,"name":"Blue","rank":2},{"id":167773,"name":"Green","rank":1},{"id":167772,"name":"Red","rank":0}]}`,
			StatusCode: http.StatusOK,
			Result: &copperv1.CustomFieldDefinition{
				Id:          utils.Int64Addr(126240),
				Name:        "Color Option",
				DataType:    "Dropdown",
				AvailableOn: []string{"opportunity", "project"},
				Options: []*copperv1.CustomFieldDefinitionOption{
					{
						Id:   utils.Int64Addr(167776),
						Name: "Yellow",
						Rank: 4,
					},
					{
						Id:   utils.Int64Addr(167775),
						Name: "Orange",
						Rank: 3,
					},
					{
						Id:   utils.Int64Addr(167774),
						Name: "Blue",
						Rank: 2,
					},
					{
						Id:   utils.Int64Addr(167773),
						Name: "Green",
						Rank: 1,
					},
					{
						Id:   utils.Int64Addr(167772),
						Name: "Red",
						Rank: 0,
					},
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", CustomFieldDefinitionsPath, test.ID), test.StatusCode, []byte(test.Body))

		result, err := FetchCustomFieldDefinitionByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}
