package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// OpportunitiesPath on API
	OpportunitiesPath = "/opportunities"
	// OpportunitiesSearchPath on API
	OpportunitiesSearchPath = "/opportunities/search"
)

var (
	// Opportunities is the global opportunities service
	Opportunities OpportunitiesService
)

// OpportunitiesService interface
type OpportunitiesService interface {
	// Create opportunity and return a pointer of Opportunity and nil
	// or nil and an error
	Create(copperv1.Opportunity) (*copperv1.Opportunity, error)
	// Delete opportunity matching id and return a pointer of DeleteResponse
	// and nil or nil and an error
	Delete(int64) (*copperv1.DeleteResponse, error)
	// Search returns a slice of Opportunity matching params and nil
	// or nil and an error. Does not handle pagination.
	Search(copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error)
	// Search returns a slice of Opportunity matching params and nil
	// or nil and an error. Handles pagination.
	SearchAll(copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error)
	// Update opportunity matching id with data and return a pointer of
	// Opportunity and nil or nil and an error
	Update(int64, copperv1.Opportunity) (*copperv1.Opportunity, error)
}

// OpportunityCreateFunc function header
type OpportunityCreateFunc func(copperv1.Opportunity) (*copperv1.Opportunity, error)

// OpportunityDeleteFunc function header
type OpportunityDeleteFunc func(int64) (*copperv1.DeleteResponse, error)

// OpportunitiesSearchFunc function header
type OpportunitiesSearchFunc func(copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error)

// OpportunityUpdateFunc function header
type OpportunityUpdateFunc func(int64, copperv1.Opportunity) (*copperv1.Opportunity, error)

type opportunitiesService struct {
	createFunc OpportunityCreateFunc
	deleteFunc OpportunityDeleteFunc
	searchFunc OpportunitiesSearchFunc
	updateFunc OpportunityUpdateFunc
}

// NewOpportunitiesService returns an instance of opportunitiesService that
// implements the OpportunitiesService interface
func NewOpportunitiesService(client Client) OpportunitiesService {
	return NewOpportunitiesServiceWithFunc(
		opportunityCreateFunc(client),
		opportunityDeleteFunc(client),
		opportunitiesSearchFunc(client),
		opportunityUpdateFunc(client),
	)
}

// NewOpportunitiesServiceWithFunc returns an instance of opportunitiesService
// that implements the OpportunitiesService interface and has custom functions set
func NewOpportunitiesServiceWithFunc(createFunc OpportunityCreateFunc, deleteFunc OpportunityDeleteFunc, searchFunc OpportunitiesSearchFunc, updateFunc OpportunityUpdateFunc) OpportunitiesService {
	return &opportunitiesService{
		createFunc: createFunc,
		deleteFunc: deleteFunc,
		searchFunc: searchFunc,
		updateFunc: updateFunc,
	}
}

// Create creates an opportunity and returns a pointer of opportunity and nil or nil and an error
func (os *opportunitiesService) Create(data copperv1.Opportunity) (*copperv1.Opportunity, error) {
	return os.createFunc(data)
}

func opportunityCreateFunc(client Client) OpportunityCreateFunc {
	return func(data copperv1.Opportunity) (*copperv1.Opportunity, error) {
		resp, err := client.Post(OpportunitiesPath, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Opportunity{}
		return result, json.Unmarshal(resp, &result)
	}
}

// Update opportunity with id with data and return a pointer of opportunity and nil or nil and an error
func (os *opportunitiesService) Update(id int64, data copperv1.Opportunity) (*copperv1.Opportunity, error) {
	return os.updateFunc(id, data)
}

func opportunityUpdateFunc(client Client) OpportunityUpdateFunc {
	return func(id int64, data copperv1.Opportunity) (*copperv1.Opportunity, error) {
		path := fmt.Sprintf("%s/%d", OpportunitiesPath, id)

		resp, err := client.Put(path, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Opportunity{}
		return result, json.Unmarshal(resp, &result)
	}
}

// Delete opportunity with id and return a pointer of DeleteResponse and nil or nil and an error
func (os *opportunitiesService) Delete(id int64) (*copperv1.DeleteResponse, error) {
	return os.deleteFunc(id)
}

func opportunityDeleteFunc(client Client) OpportunityDeleteFunc {
	return func(id int64) (*copperv1.DeleteResponse, error) {
		return deleteResourceByIDFunc(client)(OpportunitiesPath, id)
	}
}

// Search for opportunities matching params and return a slice of Opportunities and nil or nil and an error
func (os *opportunitiesService) Search(params copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error) {
	return os.searchFunc(params)
}

func opportunitiesSearchFunc(client Client) OpportunitiesSearchFunc {
	return func(params copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error) {
		data, err := client.Post(OpportunitiesSearchPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Opportunity{}
		return result, json.Unmarshal(data, &result)
	}
}

func (os *opportunitiesService) SearchAll(params copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error) {
	result := []*copperv1.Opportunity{}

	params.PageNumber = 1
	params.PageSize = MaxRequestPageSize

	done := false
	for !done {
		opportunities, err := SearchOpportunities(params)
		if err != nil {
			return nil, err
		}

		result = append(result, opportunities...)
		done = int64(len(opportunities)) < params.PageSize

		params.PageNumber++
	}

	return result, nil
}

// CreateOpportunity using data and return a pointer of Opportunity and nil
// or nil and an error
func CreateOpportunity(data copperv1.Opportunity) (*copperv1.Opportunity, error) {
	return Opportunities.Create(data)
}

// DeleteOpportunity deletes the opportuniity matching id and returns a pointer of DeleteResponse and nil
// or nil and an error
func DeleteOpportunity(id int64) (*copperv1.DeleteResponse, error) {
	return Opportunities.Delete(id)
}

// SearchOpportunities returns a slice of Opportunity matching params and nil
// or nil and an error. Does not handle pagination.
func SearchOpportunities(params copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error) {
	return Opportunities.Search(params)
}

// SearchAllOpportunities returns a slice of Opportunity matching params and nil
// or nil and an error. Handles pagination.
func SearchAllOpportunities(params copperv1.OpportunitySearchParams) ([]*copperv1.Opportunity, error) {
	return Opportunities.SearchAll(params)
}

// UpdateOpportunity updates the Opportunity matching id using data and returns a pointer of Opportunity and nil
// or nil and an error
func UpdateOpportunity(id int64, data copperv1.Opportunity) (*copperv1.Opportunity, error) {
	return Opportunities.Update(id, data)
}
