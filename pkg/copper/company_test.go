package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewCompaniesService(t *testing.T) {
	cs := NewCompaniesService(nil)
	assert.NotNil(t, cs)
}

func TestSearchCompanies(t *testing.T) {
	tt := []struct {
		Params     copperv1.CompanySearchParams
		Body       []byte
		StatusCode int
		Result     []*copperv1.Company
		Error      error
	}{
		{
			Body:       []byte(`[{"id":12345,"name":"ACME Labs","address":{"street":"Mainstreet","city":"ACME Town","state":"ACME State","postal_code":"01234","country":"ACME Land"},"details":"Some details","phone_numbers":[{"number":"+1234567890","category":"work"}],"websites":[{"url":"https://www.acmelabs.com","category":"work"}]}]`),
			StatusCode: http.StatusOK,
			Result: []*copperv1.Company{
				{
					Id:   utils.Int64Addr(12345),
					Name: "ACME Labs",
					Address: &copperv1.Address{
						Street:     "Mainstreet",
						City:       "ACME Town",
						State:      "ACME State",
						PostalCode: "01234",
						Country:    "ACME Land",
					},
					Details: "Some details",
					PhoneNumbers: []*copperv1.PhoneNumber{
						{
							Number:   "+1234567890",
							Category: "work",
						},
					},
					Websites: []*copperv1.Website{
						{
							Url:      "https://www.acmelabs.com",
							Category: "work",
						},
					},
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(CompaniesSearchPath, test.StatusCode, []byte(test.Body))

		result, err := SearchCompanies(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestFetchCompanyByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		StatusCode int
		Error      error
		Result     *copperv1.Company
	}{
		{
			ID:         -1,
			Body:       []byte(`{"success":false,"status":404,"message":"Resource not found"}`),
			StatusCode: http.StatusNotFound,
			Error:      ErrResourceNotFound,
			Result:     nil,
		},
		{
			ID:         9607580,
			Body:       []byte(`{"id":9607580,"name":"Dunder Mifflin (sample)","email_domain":"dundermifflin.com"}`),
			StatusCode: http.StatusOK,
			Error:      nil,
			Result: &copperv1.Company{
				Id:          utils.Int64Addr(9607580),
				Name:        "Dunder Mifflin (sample)",
				EmailDomain: "dundermifflin.com",
			},
		},
	}

	for i, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", CompaniesPath, test.ID), test.StatusCode, []byte(test.Body))

		result, err := FetchCompanyByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}
		server.Close()
	}
}

func TestUpdateCompany(t *testing.T) {
	tt := []struct {
		ID         int64
		Data       copperv1.Company
		Body       []byte
		StatusCode int
		Result     *copperv1.Company
		Error      error
	}{
		{
			Body:       []byte(`{"id":12345,"name":"ACME Labs","address":{"street":"Mainstreet","city":"ACME Town","state":"ACME State","postal_code":"01234","country":"ACME Land"},"details":"Some details","phone_numbers":[{"number":"+1234567890","category":"work"}],"websites":[{"url":"https://www.acmelabs.com","category":"work"}]}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.Company{
				Id:   utils.Int64Addr(12345),
				Name: "ACME Labs",
				Address: &copperv1.Address{
					Street:     "Mainstreet",
					City:       "ACME Town",
					State:      "ACME State",
					PostalCode: "01234",
					Country:    "ACME Land",
				},
				Details: "Some details",
				PhoneNumbers: []*copperv1.PhoneNumber{
					{
						Number:   "+1234567890",
						Category: "work",
					},
				},
				Websites: []*copperv1.Website{
					{
						Url:      "https://www.acmelabs.com",
						Category: "work",
					},
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", CompaniesPath, test.ID), test.StatusCode, test.Body)

		result, err := UpdateCompany(test.ID, test.Data)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}
