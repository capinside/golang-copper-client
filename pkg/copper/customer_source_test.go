package copper

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewCustomerSourcesService(t *testing.T) {
	css := NewCustomerSourcesService(nil)
	assert.NotNil(t, css)
}

func TestGetCustomerSources(t *testing.T) {
	tt := []struct {
		Body       []byte
		StatusCode int
		Result     []*copperv1.CustomerSource
		Error      error
	}{
		{
			Body:       []byte(`[{"id":331240,"name":"Email"},{"id":331241,"name":"Cold Call"},{"id":331242,"name":"Advertising"}]`),
			StatusCode: http.StatusOK,
			Result: []*copperv1.CustomerSource{
				{
					Id:   utils.Int64Addr(331240),
					Name: "Email",
				},
				{
					Id:   utils.Int64Addr(331241),
					Name: "Cold Call",
				},
				{
					Id:   utils.Int64Addr(331242),
					Name: "Advertising",
				},
			},
		},
	}

	for _, test := range tt {
		server := setupTest(CustomerSourcesPath, test.StatusCode, test.Body)

		result, err := GetCustomerSources()
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}
