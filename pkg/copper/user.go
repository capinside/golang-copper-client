package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// UsersPath on API
	UsersPath = "/users"
	// UsersSearchPath on API
	UsersSearchPath = "/users/search"
)

var (
	// Users is the global users service
	Users UsersService
)

// UsersService interface
type UsersService interface {
	Search(copperv1.UserSearchParams) ([]*copperv1.User, error)
	SearchAll(copperv1.UserSearchParams) ([]*copperv1.User, error)
	FetchByID(int64) (*copperv1.User, error)
}

// UserSearchFunc function header
type UserSearchFunc func(copperv1.UserSearchParams) ([]*copperv1.User, error)

// UserFetchByIDFunc function header
type UserFetchByIDFunc func(int64) (*copperv1.User, error)

type usersService struct {
	fetchByIDFunc UserFetchByIDFunc
	searchFunc    UserSearchFunc
}

// NewUsersService returns an instance of usersService that implements the
// UsersService interface
func NewUsersService(client Client) UsersService {
	return NewUsersServiceWithFunc(
		userFetchByIDFunc(client),
		usersSearchFunc(client),
	)
}

// NewUsersServiceWithFunc returns an instance of usersService that implements
// the UsersService interface and has fetchByIDFunc and searchFunc set
func NewUsersServiceWithFunc(fetchByIDFunc UserFetchByIDFunc, searchFunc UserSearchFunc) UsersService {
	return &usersService{
		fetchByIDFunc: fetchByIDFunc,
		searchFunc:    searchFunc,
	}
}

// Search returns a slice of Users matching params and nil or nil and an error
func (us *usersService) Search(params copperv1.UserSearchParams) ([]*copperv1.User, error) {
	return us.searchFunc(params)
}

func usersSearchFunc(client Client) UserSearchFunc {
	return func(params copperv1.UserSearchParams) ([]*copperv1.User, error) {
		data, err := client.Post(UsersSearchPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.User{}
		return result, json.Unmarshal(data, &result)
	}
}

func (us *usersService) SearchAll(params copperv1.UserSearchParams) ([]*copperv1.User, error) {
	result := []*copperv1.User{}

	params.PageNumber = 1
	params.PageSize = MaxRequestPageSize

	done := false
	for !done {
		users, err := us.Search(params)
		if err != nil {
			return nil, err
		}

		result = append(result, users...)
		done = int64(len(users)) < params.PageSize
		params.PageNumber++
	}
	return result, nil
}

// FetchByID returns the user matching id and nil or nil and an error
func (us *usersService) FetchByID(id int64) (*copperv1.User, error) {
	return us.fetchByIDFunc(id)
}

func userFetchByIDFunc(client Client) UserFetchByIDFunc {
	return func(id int64) (*copperv1.User, error) {
		path := fmt.Sprintf("%s/%d", UsersPath, id)

		data, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.User{}
		return result, json.Unmarshal(data, &result)
	}
}

// FetchUserByID fetches the user matching ID and returns a pointer of User and nil
// or nil and an error
func FetchUserByID(id int64) (*copperv1.User, error) {
	return Users.FetchByID(id)
}

// SearchUsers returns a slice of User matching params and nil
// or nil and an error. Does not handle pagination.
func SearchUsers(params copperv1.UserSearchParams) ([]*copperv1.User, error) {
	return Users.Search(params)
}

// SearchAllUsers returns a slice of User matching params and nil
// or nil and an error. Handles pagination.
func SearchAllUsers(params copperv1.UserSearchParams) ([]*copperv1.User, error) {
	return Users.SearchAll(params)
}
